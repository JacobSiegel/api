'use strict';

var express = require('express'),
    mongoose = require('mongoose'),
    http = require('http'),
    path = require('path'),
    messageModel = require('./Models/MessageModel'),
    messageRoute = require('./Routes/MessageRoutes'),
    bodyParser = require('body-parser'),
    routes = require('./Routes/routes')

var app = express()

app.set('port', process.env.PORT || 3000)
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/', routes)

var uriString = 'mongodb://jacobsiegel35:jacobsiegel35@ds037283.mlab.com:37283/social_group_chat_app_db' || process.env.MONGOLAB_URI

mongoose.connect(uriString, function(e) {
    if (e) {
        console.log('error')
    }
})

var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function callback () {
    console.log('Successsfully connected to mongodb hosted by Google Cloud and mlab :)')
})

// server is created with express
var server = app.listen(app.get('port'), function() {
    console.log("Express server listening on port " + app.get('port'))
})















// create a server with http/node.js
// http.createServer(app).listen(app.get('port'), function(){
//   console.log("Express server listening on port " + app.get('port'));
// });

// the following middle ware has to be installed manually. Not included in the latest express.
// app.use(serve-favicon);
// app.use(express.logger('dev'));
// app.use(express.bodyParser());
// app.use(express.methodOverride());
// app.use(app.router);
// app.use(express.errorHandler());
// app.get('/message', messageRoute.all);
// app.get('/message/:id', messageRoute.findById);
// app.put('/message/:id', messageRoute.update);
// app.delete('/message/:id', messageRoute.delete)
// app.post('/message', messageRoute.newMessage);
