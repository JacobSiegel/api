'use strict';

var mongoose = require('mongoose'),
    Message = mongoose.model('Message');

exports.all = function(req, res) {
    Message.find(function(e, msg) {
        if (e) {
          return res.render('Error occurred');
        }
        res.send(msg);
    });
};

exports.findById = function(req, res) {
    Message.findById( req.params.id, function(e, msg) {
      if (err) {
        res.send('Error occurred');
        return console.log(e);
      }
      res.send(msg);
    });
};

exports.newMessage = function(req,res) {
    var msg = new Message(req.body);

    emp.save(function(e) {
        if (e) {
          res.send('Error occurred');
          return console.log(e);
        }
        res.send(msg);
    });
}

exports.update = function(req, res) {
    Message.findById(req.params.id, function(e, msg) {
        if (!msg) {
            res.send('Message not found with given id');
        } else {
            if (msg.__v != req.body.__v) {
                return res.send('Please use the update message details as ' + msg);
            }
            msg.set(req.body)
            if (msg.isModified()) {
                msg.increment();

                msg.save(function(e) {
                    if (e) {
                        res.send('Error occurred');
                        return console.log(e);
                    }
                    res.send(msg);
                });
            } else {
                res.send(msg);
            }
        }
    });
};

exports.delete = function(req, res){
    Message.findById(req.params.id, function(e, msg) {
        // if(!employee){
        //     return res.send('Todo not found with given id');
        // }
        msg.remove(function(e) {
            if (e) {
                res.send('Error occurred');
                return console.log(e);
            }
            res.send('Deleted')
        });
    });
};
