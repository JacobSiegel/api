'use strict';

var mongoose = require('mongoose'),
    express = require('express'),
    Message = mongoose.model('Message')

var router = express.Router()

// Middleware
router.use(function timeLog(req, res, next) {
    console.log('Request received at: %s', Date.now())
    next()
})

// main
router.get('/', function(req, res) {
    res.json({message: "Welcome to my REST API!"})
})

// GET all messages (using a GET at http://localhost:3000/message)
router.get('/messages', function(req, res) {
    Message.find(function(e, msg) {
        if (e)
            return res.sender('Error occurred. Please try again')

        res.json(msg)
    })
})

// create new message
router.post('/messages', function(req, res) {
    var message = new Message()

    message.text = req.body.text

    message.save(function(e) {
        if (e)
            return res.sender('Error occurred. Please try again')

        res.json({message: 'Message created successfully'})
    })
})

// get single message
router.get('/messages/:message_id', function(req, res) {
    Message.findById(request.params.message_id, function(e, msg) {
        if (e)
            return res.sender('Error occurred. Please try again')

        response.json(msg)
    })
})

// update message
router.put('/messages/:message_id', function(req, res) {
    Message.findById(req.params.message_id, function(e, msg) {
        if (e)
            return res.sender('Error occurred. Please try again')

        msg.text = req.body.text

        msg.save(function(e) {
            if (e)
                return res.sender('Error occurred. Please try again')

            res.json({message: 'Message updated successfully'})
        })
    })
})

// Delete message with id (using a DELETE at http://localhost:8080/messages/:message_id)
router.delete('/messages/:message_id', function(req, res) {
    Message.remove({_id: req.params.message_id}, function(e, msg) {
        if (e)
            return res.sender('Error occurred. Please try again')

        res.json({ message: 'Successfully deleted message'})
    })
})

module.exports = router
